from app import db
from passlib.hash import pbkdf2_sha256 as sha256
from datetime import datetime

class Book(db.Model):
  id = db.Column(db.Integer, primary_key=True, nullable=False)
  judul = db.Column(db.String(50), nullable=False)
  tahun_terbit = db.Column(db.Integer, nullable=False)
  deskripsi = db.Column(db.String(100), nullable=False)

  def save(self):
    db.session.add(self)
    db.session.commit()
  
  def getAll(self):
    return Book.query.all()

  def getOne(self, id):
    return Book.query.filter_by(id=id).first()
  def delete(self):
    db.session.delete(self)
    db.session.commit()

class User(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  username = db.Column(db.String(120), unique=True, nullable=False)
  name = db.Column(db.String(50), nullable=False)
  password = db.Column(db.String(120), nullable=False)
  admin = db.Column(db.Boolean, nullable=False)
  created_at = db.Column(db.DateTime, nullable=False, default=datetime.now())

  @staticmethod
  def adminreturn(admin):
    if admin:
      return 1
    return 0
    

  @staticmethod
  def genereate_password(password):
    return sha256.hash(password)
  @staticmethod
  def verify_hash(password, hash):
    return sha256.verify(password, hash)

  def save(self):
    db.session.add(self)
    db.session.commit()
  @classmethod
  def find(c, username):
    return c.query.filter_by(username=username).first()

class RevokedTokenModel(db.Model):
  __tablename__='revoked_tokens'
  id = db.Column(db.Integer, primary_key=True)
  jti = db.Column(db.String(120))

  def add(self):
    db.session.add(self)
    db.session.commit()
  @classmethod
  def is_jti_blacklisted(cls, jti):
    cek = cls.query.filter_by(jti=jti).first()
    return bool(cek)
  