from flask_restful import Resource, reqparse
from app.models.models import Book
from flask_jwt_extended import jwt_required
from app import db

parser = reqparse.RequestParser()
parser.add_argument('judul')
parser.add_argument('tahun_terbit')
parser.add_argument('deskripsi')

class BookController(Resource):
  @jwt_required
  def post(self):
    data = parser.parse_args()
    try:
      book = Book(judul=data['judul'], tahun_terbit=data['tahun_terbit'], deskripsi=data['deskripsi'])
      book.save()
      return {'message':'success'}
    except Exception as e:
      print(data)
      return {'message':str(e)}, 500
  @jwt_required
  def get(self):
    def to_json(x):
      return{
        'id':x.id,
        'judul':x.judul,
        'tahun_terbit':x.tahun_terbit,
        'deskripsi':x.deskripsi
      }
    return  list(map(lambda x: to_json(x), Book().getAll()))
  @jwt_required
  def put(self, id):
    try:
      data = parser.parse_args()
      book = Book().getOne(id)
      book.judul = data['judul']
      book.tahun_terbit = data['tahun_terbit']
      book.deskripsi = data['deskripsi']
      db.session.commit()
      return {'message':'success updated'}
    except Exception as e:
      return {'message':str(e)}, 403
  @jwt_required
  def delete(self, id):
    try:
      book = Book().getOne(id)
      book.delete()
      return {'message':'success delete'}
    except Exception as e:
      return {'message':str(e)}

    

class BookIdController(Resource):
  @jwt_required
  def get(self, id):
    def to_json(x):
      if x==None:
        return {'message':'not found'}
      return{
        'id':x.id,
        'judul':x.judul,
        'tahun_terbit':x.tahun_terbit,
        'deskripsi':x.deskripsi
      }
    return to_json(Book().getOne(id))
        
