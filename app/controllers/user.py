from flask_restful import Resource, reqparse
from app.models.models import User
from flask_jwt_extended import (create_access_token, create_refresh_token, get_jwt_identity, jwt_required, get_raw_jwt, jwt_refresh_token_required)
from app.models.models import RevokedTokenModel

parser = reqparse.RequestParser()
parser.add_argument('username')
parser.add_argument('name')
parser.add_argument('password')
parser.add_argument('admin')

class UserRegistration(Resource):
  def post(self):
    data = parser.parse_args()
    if User().find(data['username']):
      return {'msg':'User {} exis '.format(data['username'])}, 403
    user = User(username=data['username'], name=data['name'], password=User.genereate_password(data['password']), admin=User.adminreturn(data['admin']))
    try:
      user.save()
      access_token = create_access_token(identity=data['username'])
      refresh_token = create_refresh_token(identity=data['username'])
      return {
        'user':data['username'],
        'access_token':access_token,
        'refresh_token':refresh_token
      }
    except Exception as e:
      return {'msg': str(e)}, 500


class UserLogin(Resource):
  def post(self):
    data = parser.parse_args()
    current_user = User().find(data['username'])
    if not current_user:
      return {'msg':'user {} dosent exist'.format(data['username'])},403
    if User().verify_hash(data['password'], current_user.password):
      access_token = create_access_token(identity=data['username'])
      refresh_token = create_refresh_token(identity=data['username'])
      return {
        'access_token':access_token,
        'refresh_token': refresh_token,
        'admin':current_user.admin
      }
    return {'msg':'wrong password'}, 403
        



class UserLogoutRefresh(Resource):
  @jwt_refresh_token_required
  def get(self):
    jti = get_raw_jwt()['jti']
    try:
      revoked_token = RevokedTokenModel(jti=jti)
      revoked_token.add()
      return {'msg':'refresh tomen has been revoked'}
    except Exception as e:
      return {'msg':str(e)}, 500


class TokenRefresh(Resource):
  @jwt_refresh_token_required
  def get(self):
    current_user = get_jwt_identity()
    access_token = create_access_token(identity=current_user)
    return {'token':access_token}

class UserLogout(Resource):
  @jwt_required
  def get(self):
    jti = get_raw_jwt()['jti']
    try:
      revoked_token = RevokedTokenModel(jti=jti)
      revoked_token.add()
      return {'msg':'Token has been revoked'}
    except Exception as e:
      return {'message': str(e)}
  
    
