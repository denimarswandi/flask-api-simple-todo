from flask import Flask, jsonify
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy
from app.config import Config
from flask_migrate import Migrate
from flask_cors import CORS
from flask_jwt_extended import JWTManager


app = Flask(__name__)
app.config.from_object(Config)
api = Api(app)
jwt = JWTManager(app)
db = SQLAlchemy(app) 
migrate = Migrate(app, db)
cors = CORS(app, resources={r"*":{"origins":"*"}})

@app.errorhandler(404)
def page_not_found(e):
  return jsonify({'message':'url not found'}), 404

from app.models.models import RevokedTokenModel

@jwt.token_in_blacklist_loader
def check_token(decrypted_token):
  jti = decrypted_token['jti']
  return RevokedTokenModel.is_jti_blacklisted(jti)

from app.routes import routes
