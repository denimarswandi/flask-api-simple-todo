from app import api
from app.controllers.book import BookController, BookIdController
from app.controllers.user import UserRegistration, UserLogout, UserLogoutRefresh, TokenRefresh, UserLogin

api.add_resource(BookController, '/book','/book/<id>',endpoint='book')
api.add_resource(BookIdController,'/bookid/<id>', endpoint='bookid')
api.add_resource(UserLogin,'/user/login', endpoint='login')
api.add_resource(UserRegistration, '/user/register', endpoint='register')
api.add_resource(UserLogout,'/user/logout',endpoint='logout')
api.add_resource(UserLogoutRefresh,'/user/logout/refresh', endpoint='logout_refresh')
api.add_resource(TokenRefresh, '/user/refresh/token', endpoint='token_refresh')
