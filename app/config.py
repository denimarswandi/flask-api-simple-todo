import os

class Config(object):
  SECRET_KEY = os.environ.get('SECRET_KEY') or 'apalahdisini'
  SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'mysql+pymysql://root:root@localhost/book'
  JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY') or 'ceritanyainijwtsecret'
  SQLALCHEMY_TRACK_MODIFICATIONS = False
  JWT_BLACKLIST_ENABLED = True
  JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']
  PROPAGATE_EXCEPTIONS = True